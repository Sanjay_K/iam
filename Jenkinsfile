pipeline {
    agent {
        label "Agent"
    }
    tools {
        jdk "jdk 17"
        maven "Maven 3.6.3"
    }
    environment {
        APP_NAME = "iam"
        RELEASE = "1.0.0"
        DOCKER_USER = "sanjayk43"
        DOCKER_PASS = "dockerhub"
        IMAGE_NAME = "sanjayk43" + "/" + "iam"
        IMAGE_TAG = "${RELEASE}-${BUILD_NUMBER}"
    }
    stages {
        stage("clean workspace"){
            steps {
                cleanWs()
            }
        }
         stage("checkout scm"){
            steps {
                checkout scmGit(branches: [[name: '*/master']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/Sanjay_K/iam.git']])
            }
        }
         stage("build stage"){
            steps {
                sh "mvn clean package -DskipTests"
            }
        }

         stage("sonar checks"){
            steps {
                script {
                withSonarQubeEnv(credentialsId: 'jenkins-sonar') {
                     sh "mvn sonar:sonar"
                }    
            }
        }
        }
        stage("quality gate"){
            steps {
                script {
                waitForQualityGate abortPipeline: false, credentialsId: 'jenkins-sonar' 
            }
        }
        }
        stage ("docker build") {
            steps {
                script {
                    docker.withRegistry('',DOCKER_PASS) {
                        // Remove old images from docker repository
                        sh "docker images --format '{{.Repository}}:{{.Tag}}' | grep ${IMAGE_NAME} | grep -v ${RELEASE}-${BUILD_NUMBER} | grep -v latest | xargs -I {} docker rmi {} || true"
                        docker_image = docker.build "${IMAGE_NAME}"
                    }
                }
            }
        }
        stage ("Trivy image scan") {
            steps {
                script {
                    sh "trivy image ${docker_image.id} > trivy.txt"
                }
            }
        }
        stage ("PUSH docker image") {
            steps {
                script {
                    docker.withRegistry('',DOCKER_PASS) {
                        docker_image.push("${IMAGE_TAG}")
                        docker_image.push('latest')
                    }
                }
            }
        }
    }
    post {
    always {
        emailext attachLog: true,
            subject: "'${currentBuild.result}'",
            body: "Project: ${env.JOB_NAME}<br/>" +
                "Build Number: ${env.BUILD_NUMBER}<br/>" +
                "URL: ${env.BUILD_URL}<br/>",
            to: 'ksanj43@gmail.com',
            attachmentsPattern: 'trivy.txt'
        }
    }
}