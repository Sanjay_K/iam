package com.ik.iam.model;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "user")
public class User {
    @Id
    @JsonIgnore
    private String id;

    @Field(name = "User_Id")
    private long UserId;

    @Indexed(unique = true)
    private String name;
    
    private String email;
    private String password;

    private String verificationToken;
    private boolean verified;
    private LocalDateTime createdAt;
    private LocalDateTime expiryAt;
    private LocalDateTime confirmedAt;
    private boolean AccountStatus;
    
    private String resetPasswordToken;
    @Field(name = "resetPasswordTokenCreatedAt")
    private LocalDateTime resetPasswordTokenCreatedAt;
    @Field(name = "resetPasswordTokenExpiryAt")
    private LocalDateTime resetPasswordTokenExpiryAt;

    private boolean isLoggedIn; 
    
    private List<Role> roles;
    
    private List<Policy> policies;
    
    private List<Group> groups;

    public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public LocalDateTime getResetPasswordTokenCreatedAt() {
        return resetPasswordTokenCreatedAt;
    }

    public void setResetPasswordTokenCreatedAt(LocalDateTime resetPasswordTokenCreatedAt) {
        this.resetPasswordTokenCreatedAt = resetPasswordTokenCreatedAt;
    }

    public LocalDateTime getResetPasswordTokenExpiryAt() {
        return resetPasswordTokenExpiryAt;
    }

    public void setResetPasswordTokenExpiryAt(LocalDateTime resetPasswordTokenExpiryAt) {
        this.resetPasswordTokenExpiryAt = resetPasswordTokenExpiryAt;
    }
   
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getUserId() {
		return UserId;
	}
	public void setUserId(long userId) {
		UserId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerificationToken() {
		return verificationToken;
	}
	public void setVerificationToken(String verificationToken) {
		this.verificationToken = verificationToken;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	public LocalDateTime getExpiryAt() {
		return expiryAt;
	}
	public void setExpiryAt(LocalDateTime expiryAt) {
		this.expiryAt = expiryAt;
	}
	public LocalDateTime getConfirmedAt() {
		return confirmedAt;
	}
	public void setConfirmedAt(LocalDateTime confirmedAt) {
		this.confirmedAt = confirmedAt;
	}
	public boolean isAccountStatus() {
		return AccountStatus;
	}
	public void setAccountStatus(boolean accountStatus) {
		AccountStatus = accountStatus;
	}
	public String getResetPasswordToken() {
		return resetPasswordToken;
	}
	public void setResetPasswordToken(String resetPasswordToken) {
		this.resetPasswordToken = resetPasswordToken;
	}

	public List<Policy> getPolicies() {
		return policies;
	}

	public void setPolicies(List<Policy> policies) {
		this.policies = policies;
	}

	
	
	
	

	
    	
}