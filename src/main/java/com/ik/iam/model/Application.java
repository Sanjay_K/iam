package com.ik.iam.model;




import java.util.List;

//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.index.Indexed;
//import org.springframework.data.mongodb.core.mapping.Document;
//import org.springframework.data.mongodb.core.mapping.Field;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//
//@Document(collection = "applications")
//public class Application {
//	
//	@Id
//	@JsonIgnore
//	private String id;
//	@Field("App_Id")
//	private long appId;
//	
//	@Indexed(unique = true)
//	private String appName;
//	private String appDesc;
//	
//    public Application() {
//		
//	}
//
//	public Application(String id, long appId, String appName, String appDesc) {
//		super();
//		this.id = id;
//		this.appId = appId;
//		this.appName = appName;
//		this.appDesc = appDesc;
//	}
//
//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public long getAppId() {
//		return appId;
//	}
//
//	public void setApplicationId(long appId) {
//		this.appId = appId;
//	}
//
//	public String getAppName() {
//		return appName;
//	}
//
//	public void setAppName(String appName) {
//		this.appName = appName;
//	}
//
//	public String getAppDesc() {
//		return appDesc;
//	}
//
//	public void setAppDesc(String appDesc) {
//		this.appDesc = appDesc;
//	}
//
//
//	
//	
//}



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "applications")
public class Application {
    
    @Id
    @JsonIgnore
    private String id;

    @Field("App_Id")
    private long appId;

    @Indexed(unique = true)
    private String appName;
    
    private String appDesc;
    
    
    private List<Permission> permissions; 
    
    
    public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public long getAppId() {
		return appId;
	}


	public void setAppId(long appId) {
		this.appId = appId;
	}


	public String getAppName() {
		return appName;
	}


	public void setAppName(String appName) {
		this.appName = appName;
	}


	public String getAppDesc() {
		return appDesc;
	}


	public void setAppDesc(String appDesc) {
		this.appDesc = appDesc;
	}


	public List<Permission> getPermissions() {
		return permissions;
	}


	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}


	public Application(String id, long appId, String appName, String appDesc, List<Permission> permissions) {
		super();
		this.id = id;
		this.appId = appId;
		this.appName = appName;
		this.appDesc = appDesc;
		this.permissions = permissions;
	}


	public Application() {
		super();
	}


	

    
    
   

}
