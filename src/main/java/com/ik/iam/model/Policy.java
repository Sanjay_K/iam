package com.ik.iam.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;


import org.springframework.data.mongodb.core.mapping.DBRef; // For MongoDB
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference; // Alternative to DBRef

@Document(collection="policy")
public class Policy {
	
	
	@Id
	@JsonIgnore
	private String id;
	
	@Field(name="policy_Id")
	private long policyId;
	
	private String policyName;
	
	private String policyDescription;
	
	 
	private List<Application> applications;  

	private List<Permission> permissions; 
	
	 

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getPolicyDescription() {
		return policyDescription;
	}

	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}

	public List<Application> getApplications() {
		return applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	

}
