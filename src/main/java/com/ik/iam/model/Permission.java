package com.ik.iam.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "permissions")
public class Permission {
	
	@Id
	@JsonIgnore
	private String id;
	
	@Field("Permission_Id")
	private long PermissionId;
	
	private String permissionName;
	private String permission_Desc;
	
	public Permission() {
		
	}

	public Permission(String id, long permissionId, String permissionName, String permission_Desc) {
		super();
		this.id = id;
		PermissionId = permissionId;
		this.permissionName = permissionName;
		this.permission_Desc = permission_Desc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getPermissionId() {
		return PermissionId;
	}

	public void setPermissionId(long permissionId) {
		PermissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getPermission_Desc() {
		return permission_Desc;
	}

	public void setPermission_Desc(String permission_Desc) {
		this.permission_Desc = permission_Desc;
	}
	
}

