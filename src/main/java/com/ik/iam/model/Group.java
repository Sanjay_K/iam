package com.ik.iam.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "group")
public class Group {

	@Id
	@JsonIgnore
	private String id;
	@Field("Group_Id")
	private long groupId;

	@Indexed(unique = true)
	private String groupName;
	private String groupDescription;

	private List<Policy> policies;
	
	public List<Policy> getPolicies() {
		return policies;
	}
	public void setPolicies(List<Policy> policies) {
		this.policies = policies;
	}
	public Group() {
	
	}
	public Group(String id, long groupId, String groupName, String groupDescription) {
		super();
		this.id = id;
		this.groupId = groupId;
		this.groupName = groupName;
		this.groupDescription = groupDescription;
	}


	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}


	private List<User> users;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	
}