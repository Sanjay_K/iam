package com.ik.iam.processor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.ik.iam.apiConfigs.ActionConfig;
import com.ik.iam.apiConfigs.ApiConfigProperties;


@Component
public class DeleteReqProcessor {
	private final ApplicationContext applicationContext;
	private final ApiConfigProperties apiConfigProperties;

	@Autowired
	public DeleteReqProcessor(ApplicationContext applicationContext, ApiConfigProperties apiConfigProperties) {
		this.applicationContext = applicationContext;
		this.apiConfigProperties = apiConfigProperties;
	}

	
	
	 public ResponseEntity handleDeleteAction(String action, String variable) {
		  System.out.println("Handling POST request for action: " + action);
	     ActionConfig actionConfig = apiConfigProperties.getActions().stream()
	             .filter(config -> config.getapi().equals(action))
	             .findFirst()
	             .orElse(null);

	     if (actionConfig == null) {
	    	 System.out.println(actionConfig);
	         return ResponseEntity.notFound().build();
	        
	     }

	     String method = actionConfig.getMethod();
	     System.out.println("Service class: " + actionConfig.getServiceClass());
	     System.out.println("Service method: " + actionConfig.getServiceMethod());

	     try {
	         if ("DELETE".equals(method)) {
	             Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
	             Object serviceInstance = applicationContext.getBean(serviceClass);

	             // Assume that the service method takes the variable (userId) as a parameter
	             Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), String.class);

	             // Invoke the service method and capture the result
	             Object result = serviceMethod.invoke(serviceInstance, variable);

	             // Return the result in the response
	             return ResponseEntity.ok(result);
	         } else {
	             return ResponseEntity.badRequest().body("Invalid method for action " + action);
	         }
	     } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
	             | InvocationTargetException e) {
	         e.printStackTrace(); // Print the exception for debugging
	         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                 .body("Error executing action: " + e.getMessage());
	     }
	 }
}
