package com.ik.iam.processor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.ik.iam.apiConfigs.ActionConfig;
import com.ik.iam.apiConfigs.ApiConfigProperties;

@Component
public class PutReqProcessor  {

	
	
	private final ApplicationContext applicationContext;
	private final ApiConfigProperties apiConfigProperties;

	@Autowired
	public PutReqProcessor(ApplicationContext applicationContext, ApiConfigProperties apiConfigProperties) {
		this.applicationContext = applicationContext;
		this.apiConfigProperties = apiConfigProperties;
	}

	
	//this method is working fine 
		public ResponseEntity<Object> handlePutAction(String action, String variable, Map<String, Object> requestData) {
		    // 1. Find the action configuration
			ActionConfig actionConfig = apiConfigProperties.getActions().stream()
					.filter(config -> config.getapi().equals(action)).findAny().orElse(null);

		    // 2. Verify the HTTP method
		    if (!actionConfig.getMethod().equals("PUT")) {
		        return ResponseEntity.badRequest().body("Invalid method for action " + action);
		    }

		    // 3. Load the service class and method
		    try {
		        Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
		        Object serviceInstance = applicationContext.getBean(serviceClass);
		        Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), String.class, Map.class);

		        // 4. Invoke the service method and handle response
		        Object result = serviceMethod.invoke(serviceInstance, variable, requestData);
		        if (result == null) {
		            return ResponseEntity.ok().build(); // No content to return
		        } else {
		            return ResponseEntity.ok(result); // Return the result
		        }
		    } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
		            | InvocationTargetException e) {
		        // 5. Handle exceptions
//		        logger.error("Error executing action " + action, e); // Log the error
		        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
		                .body("Error executing action: " + e.getMessage());
		    }
		}
}
