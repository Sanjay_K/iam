package com.ik.iam.processor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.ik.iam.apiConfigs.ActionConfig;
import com.ik.iam.apiConfigs.ApiConfigProperties;

//little workable
@Component
public class RequestProcessor {

	private final ApplicationContext applicationContext;
	private final ApiConfigProperties apiConfigProperties;

	@Autowired
	public RequestProcessor(ApplicationContext applicationContext, ApiConfigProperties apiConfigProperties) {
		this.applicationContext = applicationContext;
		this.apiConfigProperties = apiConfigProperties;
	}

	public ResponseEntity<String> handleAction(String action, String entity, Map<String, Object> requestData) {
//		ActionConfig actionConfig = apiConfigProperties.getActions().stream()
//				.filter(config -> config.getapi().equals(action)).findFirst().orElse(null);
		ActionConfig actionConfig = apiConfigProperties.getActions().stream().filter(config -> {
			String configEntity = config.getentity();
			return configEntity != null && configEntity.equals(entity) && config.getapi().equals(action);
		}).findFirst().orElse(null);
		if (actionConfig == null) {
			System.out.println("Action not found: " + action);
			return ResponseEntity.notFound().build();
		}

		String method = actionConfig.getMethod();
		System.out.println("Service class: " + actionConfig.getServiceClass());
		System.out.println("Service method: " + actionConfig.getServiceMethod());

		try {
			if ("POST".equals(method) || "GET".equals(method) || "PUT".equals(method) || "DELETE".equals(method)) {
				if ("GET".equals(method) && "getUsersById".equals(action)) {
					return handleGetUserById(actionConfig, (Long) requestData.get("id"));
				} else if ("GET".equals(method) && "getByName".equals(action)) {
					return handleGetByName(actionConfig, (String) requestData.get("name"));
				} else if ("GET".equals(method) && "getGroupByName".equals(action)) {
					return handleGetGroupByName(actionConfig, (String) requestData.get("groupName"));
				} else if ("GET".equals(method) && "getGroupById".equals(action)) {
					return handleGetGroupById(actionConfig, (Long) requestData.get("id"));
				} else if ("GET".equals(method) && "getAll".equals(action)) {
					return handleGetAll(actionConfig);
				} else if ("PUT".equals(method) && "handlePutById".equals(action)) {
					return handlePutById(actionConfig, (Long) requestData.get("id"));
				} else if ("PUT".equals(method) && "handlePutByName".equals(action)) {
					return handlePutByName(actionConfig, (String) requestData.get("name"));
				} 
				Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
				Object serviceInstance = applicationContext.getBean(serviceClass);
				Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), Map.class);

				Object result = serviceMethod.invoke(serviceInstance, requestData);

				// Check if the result is null
				if (result == null) {
					return ResponseEntity.ok("Action executed successfully!");
				}

				// Check if the result is a ResponseEntity
				if (result instanceof ResponseEntity) {
					return (ResponseEntity<String>) result;
				} else {
					return ResponseEntity.ok(result.toString());
				}
			} else {
				return ResponseEntity.badRequest().body("Invalid method for action " + action);
			}
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace(); // Print the exception for debugging
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error executing action: " + e.getMessage());
		}
	}

	private ResponseEntity<String> handleGetGroupByName(ActionConfig actionConfig, String groupName) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), String.class);
			ResponseEntity<String> result = (ResponseEntity<String>) serviceMethod.invoke(serviceInstance, groupName);

			serviceMethod.invoke(serviceInstance, groupName);
			return result;
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace(); // Handle the exception appropriately in your application
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error handling GET request for user by name: " + e.getMessage());

		}
	}

	private ResponseEntity<String> handleGetGroupById(ActionConfig actionConfig, Long groupId) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), Long.class);

			// Invoke the method with the groupId parameter
			Object result = serviceMethod.invoke(serviceInstance, groupId);

			// Check if the result is a ResponseEntity
			if (result instanceof ResponseEntity) {
				return (ResponseEntity<String>) result;
			} else {
				// If not, convert the result to a string and return it
				return ResponseEntity.ok(result.toString());
			}
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace(); // Handle the exception appropriately in your application
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error handling GET request for group by ID: " + e.getMessage());
		}
	}

	private ResponseEntity<String> handleGetUserById(ActionConfig actionConfig, Long id) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), Long.class);

			ResponseEntity<String> result = (ResponseEntity<String>) serviceMethod.invoke(serviceInstance, id);
			serviceMethod.invoke(serviceInstance, id);

			return result;
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace(); // Handle the exception appropriately in your application
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error handling GET request for user by ID: " + e.getMessage());
		}
	}

	private ResponseEntity<String> handleGetByName(ActionConfig actionConfig, String name) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), String.class);
			ResponseEntity<String> result = (ResponseEntity<String>) serviceMethod.invoke(serviceInstance, name);

			serviceMethod.invoke(serviceInstance, name);
			return result;
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace(); // Handle the exception appropriately in your application
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error handling GET request for user by name: " + e.getMessage());
		}

	}


	private ResponseEntity<String> handleGetAll(ActionConfig actionConfig) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod());

//			ResponseEntity<String> result = (ResponseEntity<String>) serviceMethod.invoke(serviceInstance);
			return (ResponseEntity<String>) serviceMethod.invoke(serviceInstance);


		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace(); // Handle the exception appropriately in your application
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error handling GET request for all entities: " + e.getMessage());
		}
	}

	private ResponseEntity<String> handlePutById(ActionConfig actionConfig, Long id) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod());
			serviceMethod.invoke(serviceInstance);
			return ResponseEntity.ok("update by Id Successful!");
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error to update it by id :" + e.getMessage());
		}

	}

	private ResponseEntity<String> handlePutByName(ActionConfig actionConfig, String name) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object sericeInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod());
			serviceMethod.invoke(sericeInstance);
			return ResponseEntity.ok("updated by name!");
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Error to update it by id :" + e.getMessage());
		}
	}

	private ResponseEntity<String> handleDeleteById(ActionConfig actionConfig, Long id) {
		try {
			Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
			Object serviceInstance = applicationContext.getBean(serviceClass);
			Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod());
			serviceMethod.invoke(serviceInstance);
			return ResponseEntity.ok("deleted By Id!!!!!!");
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("not deleted by Id!" + e.getMessage());

		}

	}


	public ResponseEntity<Object> handlePostAction(String action, Map<String, Object> requestData) {
	    ActionConfig actionConfig = apiConfigProperties.getActions().stream()
	            .filter(config -> config.getapi().equals(action))
	            .findFirst()
	            .orElse(null);

	    if (actionConfig == null) {
	        return ResponseEntity.badRequest().body("Action configuration not found for: " + action);
	    }

	    System.out.println("Action Config: " + actionConfig);

	    try {
	        Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
	        Object serviceInstance = applicationContext.getBean(serviceClass);

	        System.out.println("Service class: " + actionConfig.getServiceClass());
	        System.out.println("Service method: " + actionConfig.getServiceMethod());

	        Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), Map.class);

	        Object result = serviceMethod.invoke(serviceInstance, requestData);
	        if (result == null) {
	            return ResponseEntity.ok().build();
	        } else {
	            return ResponseEntity.ok(result);
	        }
	    } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
	            | InvocationTargetException e) {
	        e.printStackTrace(); // Print the stack trace for better debugging
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                .body("Error executing action: " + e.getMessage());
	    }
	}

	
	
	public ResponseEntity<String> handleDeleteAction(String action, Map<String, Object> requestData) {
	    // 1. Find the action configuration
	    ActionConfig actionConfig = apiConfigProperties.getActions().stream()
	            .filter(config -> config.getapi().equals(action))
	            .findAny()
	            .orElse(null);

	    // 2. Verify the HTTP method
	    if (!actionConfig.getMethod().equals("DELETE")) {
	        return ResponseEntity.badRequest().body("Invalid method for action " + action);
	    }

	    // 3. Load the service class and method
	    try {
	        Class<?> serviceClass = Class.forName(actionConfig.getServiceClass());
	        Object serviceInstance = applicationContext.getBean(serviceClass);
	        Method serviceMethod = serviceClass.getMethod(actionConfig.getServiceMethod(), Map.class);

	        // 4. Invoke the service method and handle response
	        return (ResponseEntity<String>) serviceMethod.invoke(serviceInstance, requestData);
	    } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
	            | InvocationTargetException e) {
	        // 5. Handle exceptions
	        // logger.error("Error executing action " + action, e); // Log the error
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                .body("Error executing action: " + e.getMessage());
	    }
	}

	
}
