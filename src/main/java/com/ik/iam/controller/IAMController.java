package com.ik.iam.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ik.iam.processor.DeleteReqProcessor;
import com.ik.iam.processor.PutReqProcessor;
import com.ik.iam.processor.RequestProcessor;
import com.ik.iam.services.AuthenticationServices;
import com.ik.iam.services.ForgotPasswordService;

@RestController
@RequestMapping("/api")
public class IAMController {

	@Autowired
	private RequestProcessor requestProcessor;

	@Autowired
	private ForgotPasswordService forgotPasswordService;

	@Autowired
	private AuthenticationServices authenticationServices;
	
	@Autowired
	private PutReqProcessor PutReqProcessor;
	@Autowired
	private DeleteReqProcessor delReqProcessor;
	
	@PostMapping(value = "/{action}/{entity}")
	public ResponseEntity handleHttpRequest(@RequestBody Map<String, Object> requestData, @PathVariable String entity,@PathVariable String action) {

//		return requestProcessor.handleAction(action, requestData);
		return requestProcessor.handleAction(action, entity,requestData);
	}
	
	
	
	@GetMapping(value = "/{action}/{entity}/{id}")
	public ResponseEntity handleHttpGetRequestWithId(@PathVariable String action, @PathVariable String entity,
			@PathVariable Long id, @RequestParam(required = false) Map<String, String> queryParams,
			@RequestBody(required = false) Map<String, Object> requestBody) {
		// Handle the scenario: Action specific endpoint + Entity + ID
		// Example: /api/getUsersById/user/123
		return constructGet(action, entity, id, null, queryParams, requestBody);
	}

	@GetMapping(value = "/{action}/{entity}/getall")
	public ResponseEntity handleHttpGetRequestForAll(@PathVariable String action, @PathVariable String entity,
			@RequestParam(required = false) Map<String, String> queryParams,
			@RequestBody(required = false) Map<String, Object> requestBody) {
		// Handle the scenario: Action specific endpoint + Entity + Get All
		// Example: /api/getall/user
		return constructGet(action, entity, null, null, queryParams, requestBody);
	}

	@GetMapping(value = "/{action}/{entity}/getByName")
	public ResponseEntity handleHttpGetRequestForName(@PathVariable String action, @PathVariable String entity,
			@RequestParam String name, @RequestParam(required = false) Map<String, String> queryParams,
			@RequestBody(required = false) Map<String, Object> requestBody) {
		// Handle the scenario: Action specific endpoint + Entity + Name
		// Example: /api/getByName/user?name=John
		return constructGet(action, entity, null, name, queryParams, requestBody);
	}
	
	@RequestMapping(value = "/{action}/{entity}", method = RequestMethod.GET)
	public ResponseEntity handleHttpGetRequestWithIdAndBody(@PathVariable String action,
//	        @PathVariable String entity,
//	        @PathVariable Long id,
			@RequestParam(required = false) Map<String, String> queryParams,
			@RequestBody(required = false) Map<String, Object> requestBody) {

		// Your logic here

		return constructGet(action, null, null, null, queryParams, requestBody);
	}

	private ResponseEntity constructGet(String action, String entity, Long id, String name,
			Map<String, String> queryParams, Map<String, Object> requestBody) {

		// Construct requestData based on the parameters
		Map<String, Object> requestData = new HashMap<>();
		if (entity != null) {
			requestData.put("entity", entity);
		}
		if (id != null) {
			requestData.put("id", id);
		}
		if (name != null) {
			requestData.put("name", name);
		}

		// Add query parameters to requestData if present
		if (queryParams != null) {
			requestData.putAll(queryParams);
		}

		// Add request body parameters to requestData if present
		if (requestBody != null) {
			requestData.putAll(requestBody);
		}

		// Delegate the handling of GET requests to RequestProcessor
		return requestProcessor.handleAction(action, entity,requestData);
	}
	

	// This method is for register via email to verify the token
	@GetMapping(value = "/verify")
	public ResponseEntity verifyUser(@RequestParam String token) {
		try {
			// Call the method in your service to verify the user based on the token
			ResponseEntity<String> verificationResult = authenticationServices.verifyRegistrationAcc(token);

			// Check the verification result and return an appropriate response
			if (verificationResult.getStatusCode().is2xxSuccessful()) {
				return ResponseEntity.ok("User verified successfully!");
			} else {
				return ResponseEntity.badRequest()
						.body("User verification failed. Reason: " + verificationResult.getBody());
			}
		} catch (Exception e) {
			// Log any exceptions for debugging
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("An error occurred during user verification. Please try again.");
		}
	}

	// This Method is to verify token while Resetting password
	@GetMapping("/verify-password-reset-token/{resetPasswordToken}")
	public ResponseEntity<String> verifyPasswordResetToken(@PathVariable String resetPasswordToken) {
		return forgotPasswordService.verifyPasswordResetToken(resetPasswordToken);
	}
	
	
	// TOdays change
	@PutMapping("/{action}/{variable}")
	public ResponseEntity handleHttpPutRequests(@PathVariable String action, @PathVariable String variable, @RequestBody Map<String, Object> requestData) {
	    System.out.println("Received PUT request. Action: " + action + ", Variable: " + variable);
	    // ... rest of the method
	    return PutReqProcessor.handlePutAction(action, variable, requestData);
	}

	
	
	@DeleteMapping("/{action}/{variable}")
	public ResponseEntity handleDeleteAction(@PathVariable String action, @PathVariable String variable) {
	    System.out.println("Received DELETE request. Action: " + action + ", Variable: " + variable);
	    return delReqProcessor.handleDeleteAction(action, variable);
	}
	
	
	
	// This method is for to post assign role to users
	@PostMapping(value = "/{action}")
	public ResponseEntity handleHttpPostRequest(@RequestBody Map<String, Object> requestData ,@PathVariable String action) {

//		return requestProcessor.handlepostAction(action, requestData);
		return requestProcessor.handlePostAction(action,requestData);
	}
	
	
	
	@DeleteMapping("/{action}")
    public ResponseEntity<String> removeUserFromRole(@RequestBody Map<String, Object> requestData,@PathVariable String action) {
		//return requestProcessor.handleAction(action, requestData);
		return requestProcessor.handleDeleteAction(action,requestData);
    }
	
	
	
	
}
