package com.ik.iam.emailService;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;

@Service
public class EmailService {
	@Autowired
	private JavaMailSender javaMailSender;

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmailService.class);

	public void sendVerificationEmail(String userEmail, String verificationToken, String subject) {
		String verificationLink = "http://192.168.0.135:8080/api/verify?token=" + verificationToken;

		String body = "Dear user,\n\n"
				+ "Thank you for registering. Please click onn the below link to activate your account:\n"
				+ "Activate Now: " + verificationLink + "\n\n" + "Link will expire in 10 minutes, See you soon.";

		// Send the email
		sendEmail(userEmail, subject, body);
	}

	private void sendEmail(String to, String subject, String body) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("yedikannavar.anil@gmail.com"); // Set your email here
		message.setTo(to);
		message.setSubject(subject);
		message.setText(body);

		try {
			javaMailSender.send(message);
			logger.info("Email sent successfully! ");
		} catch (Exception e) {
			logger.error("Error sending email to: " + to, e);
			// Handle the exception appropriately in your application
		}
	}

	public void sendPasswordResetEmail(String email, String verificationToken) {
		String verificationLink = "http://192.168.0.135:8080/api/verify-password-reset-token/" + verificationToken;

		String subject = "Password Reset Request";
		String body = "Dear user,\n\n" + "Please click on the following link to reset your password:\n"
				+ verificationLink + "\n\n"
				+ "This link will expire in a certain period, so reset your password as soon as possible.";

		// Send the email
		sendEmail(email, subject, body);
	}

	public void sendInvitationEmail(String email) {
		String registrationLink="http://localhost:8080/";
	    String subject = "Invitation to join Acme App";  // Replace with your app name
	    String body = "Dear user,\n\n" +
	                  "You've been invited to join Acme App! Please click on the following link to register:\n" +
	                  registrationLink + "\n\n" +
	                  "We're excited to have you on board.";

	    // Send the email
	    sendEmail(email, subject, body);
	}
}
