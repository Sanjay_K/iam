package com.ik.iam.validationConfigs;


public class ValidationRule {

    private int minLength;
    private int maxLength;
    private String regex;
    private String errorMessage;

    // Constructors, getters, and setters

    public int getMinLength() {
        return minLength;
    }

  
	public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

