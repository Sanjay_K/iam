package com.ik.iam.validationConfigs;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;


//@Component
//@EnableConfigurationProperties
//@ConfigurationProperties(prefix = "validation")
//@PropertySource("classpath:application-validation.yml")
@Configuration
@ConfigurationProperties(prefix = "validation")
public class ValidationConfig {
    private Map<String, Map<String, ValidationRule>> rules;

    public Map<String, Map<String, ValidationRule>> getRules() {
        return rules;
    }

    public void setRules(Map<String, Map<String, ValidationRule>> rules) {
        this.rules = rules;
    }
    
    @PostConstruct
    public void init() {
    	System.out.println(rules);
    }
}
