package com.ik.iam.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ik.iam.model.Group;
import com.ik.iam.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

	Optional<User> findByVerificationToken(String token);

	Optional<User> findByEmail(String email);

	@Query("{'email': ?0}")
	User findByEmail1(String email);

	boolean existsByEmail(String email);

	org.apache.el.stream.Optional findByresetPasswordToken(String token);

	Optional<User> findByResetPasswordToken(String token);

	Optional<User> findByResetPasswordToken(Object object);

	@Query("{ 'User_Id' : ?0 }")
	Optional<User> findByUser_Id(Long userId);

	List<User> findBynameIgnoreCase(String trim);

	@Query("{ 'User_Id' : ?0 }")
	User findByUser_Id1(Long userId);
	
	//updateByName
	@Query("{ 'name' : ?0 }")
	User findByname(String name);
	
	//TODO: today's change
//Delete User ByName
	User deleteByname(String name);
	
	@Query("{ 'User_Id' : ?0 }")
	Optional<User> findByUser_Id2(Long userId);

	

	//updateByName
		@Query("{ 'name' : ?0 }")
		Optional<User> findByname1(String name);

//		List<User> findByname1In(List<String> usernames);
		
//		List<User> findByUsernameIn(List<String> usernames);
		
		@Query("SELECT u FROM User u WHERE u.name1 IN :usernames")
		List<User> findByname1In(@Param("usernames") List<String> usernames);

		@Query("{ 'User_Id' : { $in: ?0 } }") // Update the query to use $in for multiple userIds
		List<User> findByUser_IdIn(Collection<Long> userId);
		
		List<User> findByNameIn(Collection<String> name);

		@Query("{ 'name' : { $in: ?0 } }")
		User findByName(String name);

//		User findByUserName(String name);

		



	
	

}