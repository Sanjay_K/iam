package com.ik.iam.repository;


import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.ik.iam.model.Application;

@Repository
public interface ApplicationRepository extends MongoRepository<Application, String> {

	boolean existsByAppName(String appName);


	
	
	@Query("{ 'App_Id' : ?0 }")
	Application findByApp_Id(Long appId);

	@Query("{ 'App_Id' : ?0 }")
	List<Application> findByApp_Id(List<Integer> appIds);


	Application findByAppName(String appName);

	@Query("{ 'app.id' : { $in : ?0 } }")
	Collection<String> findAllByApp_Id(List<String> ids);

	// In ApplicationRepository
	List<Application> findApplicationsByAppNameIn(List<String> appNames);

	@Query("{ 'appName' : ?0 }")
	Optional<Application> findByAppName1(String appName);
	
	
//	List<Application> findByAppName(List<String> appName);

}