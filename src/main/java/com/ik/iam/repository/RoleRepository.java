package com.ik.iam.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ik.iam.model.Role;
import com.ik.iam.model.User;


@Repository
public interface RoleRepository extends MongoRepository<Role, String> {
	boolean existsByRoleId(long roleId);

	List<Role> findByRoleNameIgnoreCase(String trim);

	Optional<Role> findByRoleName(String roleName);
	
	
	@Query("{ 'roleName' : ?0 }")
	List<Role> findByRoleName2(String roleName);
	
	@Query("{ 'roleName' : ?0 }")
	Role findByRoleName1(String roleName);

	
	@Query("{ 'role_Id' : ?0 }")
	Optional<Role> existsByrole_Id(Long roleId);
//	@Query("{ 'role_Id' : ?0 }")
//	Role findByrole_Id(long roleId);

//	@Query("{ 'role_Id' : ?0 }")
//	Role findByRole_Id(Long roleId);

	@Query("{ 'role_Id' : ?0 }")
	Role findByRole_Id(long roleId);

	@Query("{ 'role_Id' : ?0 }")
	List<Role> findByRole_Id(List<Integer> roleIds);

	
//	
//	List<Role> findByRoleNameIn(List<String> roleNames);
	
	
	


	

}