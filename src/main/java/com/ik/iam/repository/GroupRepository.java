package com.ik.iam.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.ik.iam.model.Group;
import com.ik.iam.model.User;

@Repository
public interface GroupRepository extends MongoRepository<Group, String>{

//	Group findByGroupName(String groupName);
    boolean existsByGroupName(String groupName);
    
	@Query("{ 'Group_Id' : ?0 }")
	Optional<Group> findByGroup_Id(Long groupId); // Update the method name to findByUser_Id

	void deleteBygroupName(String groupName);

	
	
	Optional<Group> findByGroupName(String groupName);


	@Query("{ 'groupName' : ?0 }")
	Group findByGroupName1(String groupName);

	List<Group> findByGroupNameIgnoreCase(String trim);
	
	@Query("{ 'Group_Id' : { $in: ?0 } }") // Update the query to use $in for multiple userIds
	List<Group> findByGroup_IdIn(Collection<Long> groupIds);

//	Collection<? extends Group> findByGroupNameIn(List<String> groupNames);
	
	List<Group> findByGroupNameIn(List<String> groupNames);

}
