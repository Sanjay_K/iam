package com.ik.iam.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.ik.iam.model.Permission;



public interface PermissionRepository extends MongoRepository<Permission, String> {

	boolean existsBypermissionName(String permissionName);

	Permission findByPermissionName(String permissionName);

	List<Permission> findByPermissionName(List<String> permissionName);

//	// In PermissionRepository
//	List<Permission> findPermissionsByPermissionNameIn(List<String> permissionNames);

	@Query("{ 'permissionName' : ?0 }")
	Optional<Permission> findByPermissionName1(String permissionName);

	List<Permission> findByPermissionNameIn(List<String> permissionNames);
	
}
