package com.ik.iam.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ik.iam.model.Policy;

@Repository
public interface PolicyRepository extends MongoRepository<Policy,String> {

	boolean existsByPolicyId(long policyId);

	Policy findByPolicyName(String policyName);

}
