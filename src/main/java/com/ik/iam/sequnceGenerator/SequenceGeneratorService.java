package com.ik.iam.sequnceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.ik.iam.model.Sequence;



@Service
public class SequenceGeneratorService {

    @Autowired
    private MongoOperations mongoOperations;

    // Define the default initial value for the sequence
    private static final long DEFAULT_INITIAL_SEQUENCE_VALUE = 1L;

    public long generateSequence(String seqName, long initialSequenceValue) {
        Query query = new Query(Criteria.where("_id").is(seqName));
        Update update = new Update().inc("seq", 1);

        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);
        options.upsert(true);

        // Try to find and update the existing sequence
        Sequence counter = mongoOperations.findAndModify(query, update, options, Sequence.class);

        // If the sequence doesn't exist, initialize it with the specified initial value
        if (counter == null) {
            counter = new Sequence();
            counter.setId(seqName);
            counter.setSeq(initialSequenceValue);
            mongoOperations.save(counter);
        }

        return counter.getSeq();
    }

    // Overloaded method with default initial value
    public long generateSequence(String seqName) {
        return generateSequence(seqName, DEFAULT_INITIAL_SEQUENCE_VALUE);
    }
}
