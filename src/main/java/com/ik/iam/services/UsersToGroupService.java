package com.ik.iam.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.management.relation.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import com.application.model.Roles;
//import com.application.services.Set;
//import com.ik.iam.actionHandler.BaseActionHandler;
import com.ik.iam.model.Group;
import com.ik.iam.model.User;
import com.ik.iam.repository.GroupRepository;
import com.ik.iam.repository.RoleRepository;
import com.ik.iam.repository.UserRepository;

@Service
public class UsersToGroupService  {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private GroupRepository groupRepository;

	//This method is to Add Groups to user
	@Transactional
	public ResponseEntity<String> addGroupsToUser(Map<String, Object> requestData) {
	    try {
	        // Retrieve userId or name from the request
	        Long userId = requestData.containsKey("userId") ? ((Integer) requestData.get("userId")).longValue() : null;
	        String name = requestData.containsKey("name") ? (String) requestData.get("name") : null;

	        // Fetch the user by userId or name
	        Optional<User> user = (userId != null) ? userRepository.findByUser_Id(userId) :
	                              (name != null) ? userRepository.findByname1(name) : Optional.empty();

	        // If no valid user is found, return an error
	        if (user.isEmpty()) {
	            return ResponseEntity.badRequest().body("Error: User not found.");
	        }

	        // Retrieve groupIds and groupNames from the request or initialize empty lists
	        List<Long> groupIds = requestData.containsKey("groupIds") ? (List<Long>) requestData.get("groupIds") : new ArrayList<>();
	        List<String> groupNames = requestData.containsKey("groupNames") ? (List<String>) requestData.get("groupNames") : new ArrayList<>();

	        // Fetch groups by their IDs or names
	        List<Group> groupsToAdd = new ArrayList<>();

	        // Fetches groups by their IDs and adds them to the groupsToAdd list if group IDs are present in the request.
	        if (!groupIds.isEmpty()) {
	            groupsToAdd.addAll(groupRepository.findByGroup_IdIn(groupIds));
	        }

	        if (!groupNames.isEmpty()) {
	            groupsToAdd.addAll(groupRepository.findByGroupNameIn(groupNames));
	        }

	        // Check if all groups exist
	        if (groupsToAdd.size() != (groupIds.size() + groupNames.size())) {
	            return ResponseEntity.badRequest().body("Error: One or more groups not found.");
	        }

	        // Initialize the groups collection in the user if null
	        if (user.get().getGroups() == null) {
	            user.get().setGroups(new ArrayList<>());
	        }

	        // Iterate over the groupsToAdd list to add groups to the user
	        for (Group group : groupsToAdd) {
	            // Check if the group is already associated with the user
	            if (user.get().getGroups().stream().anyMatch(g -> g.getGroupId() == group.getGroupId())) {
	                return ResponseEntity.badRequest().body("Error: Group with ID " + group.getGroupId() + " and name " + group.getGroupName() + " is already associated with the user.");
	            }
	        }

	        // Check if the user's account status allows group association
	        if (user.get().isAccountStatus()) {
	            // Add groups to the user
	            user.get().getGroups().addAll(groupsToAdd);

	            // Update the group's user association and save groups
	            groupRepository.saveAll(groupsToAdd);
	        } else {
	            // If the condition is not met, return an error
	            return ResponseEntity.badRequest().body("Error: Add your specific condition message here.");
	        }

	        // Save the updated user
	        userRepository.save(user.get());

	        return ResponseEntity.ok("Groups added to the user successfully.");
	    } catch (Exception e) {
	        // Return error response in case of exceptions
	        return ResponseEntity.badRequest().body("Error adding groups to the user: " + e.getMessage());
	    }
	}
	
	
	
	//This method is to Get User by UserName Along with groups 
	public ResponseEntity<Object> getUserWithGroupsByName(String name) {
	    try {
	        Optional<User> userOptional = userRepository.findByname1(name);

	        if (userOptional.isPresent()) {
	            User user = userOptional.get();

	            Map<String, Object> userMap = new HashMap<>();
	            userMap.put("userId", user.getUserId());
	            userMap.put("name", user.getName());
	            // Add other user fields as needed
	            
	            // Retrieve groups associated with the user
	            List<Map<String, Object>> groupList = user.getGroups().stream()
	                    .map(group -> {
	                        Map<String, Object> groupMap = new HashMap<>();
	                        groupMap.put("groupId", group.getGroupId());
	                        groupMap.put("groupName", group.getGroupName());
	                        groupMap.put("groupDescription", group.getGroupDescription());
	                        // Add other group fields as needed
	                        return groupMap;
	                    })
	                    .collect(Collectors.toList());

	            userMap.put("groups", groupList);
	            // Add other user fields as needed

	            return ResponseEntity.ok(userMap);
	        } else {
	            System.out.println("No user found for name: " + name);
	            // Handle the case where no user is found
	            Map<String, String> responseMap = new HashMap<>();
	            responseMap.put("message", "No user found for name: " + name);
	            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
	        }
	    } catch (Exception e) {
	        // Log any exceptions for debugging
	        e.printStackTrace();
	        // You can choose to handle the exception differently or return an empty response
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	    }
	}


}
