package com.ik.iam.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ik.iam.model.Application;
import com.ik.iam.model.Group;
import com.ik.iam.model.Permission;
import com.ik.iam.model.Policy;
import com.ik.iam.model.Role;
import com.ik.iam.model.User;
import com.ik.iam.repository.ApplicationRepository;
import com.ik.iam.repository.GroupRepository;
import com.ik.iam.repository.PermissionRepository;
import com.ik.iam.repository.PolicyRepository;
import com.ik.iam.repository.RoleRepository;
import com.ik.iam.repository.UserRepository;
import com.ik.iam.sequnceGenerator.SequenceGeneratorService;

@Service
public class PolicyServices  {

	private final PolicyRepository policyRepository;
	private final SequenceGeneratorService sequenceGeneratorService;
	
	@Autowired
    private ApplicationRepository applicationRepository;
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private RoleRepository roleRepository;


	@Autowired
	private UserRepository userRepository;

	@Autowired
    private  PermissionRepository permissionRepository;

	public PolicyServices(PolicyRepository policyRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.policyRepository = policyRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

	
	//This method is to simply add the new policy
	public ResponseEntity<Object> addPolicy(Map<String, Object> requestData) {
		try {
			// Extract policy registration data from the requestData map
			long policyId = sequenceGeneratorService.generateSequence("Policy_Id");
			String policyName = (String) requestData.get("policyName");
			String policyDescription = (String) requestData.get("policyDescription");

			// Check if the policy with the same policyId is already registered
			if (policyRepository.existsByPolicyId(policyId)) {
				throw new IllegalArgumentException("Policy with policyId " + policyId + " already registered!");
			}

			// Create a new policy entity and save it
			Policy newPolicy = new Policy();
			newPolicy.setPolicyId(policyId);
			newPolicy.setPolicyName(policyName);
			newPolicy.setPolicyDescription(policyDescription);
			Policy savedPolicy = policyRepository.save(newPolicy);

			return ResponseEntity.status(HttpStatus.CREATED).body(savedPolicy);
		} catch (Exception e) {
			// Log any exceptions for debugging
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(Collections.singletonMap("error", e.getMessage()));
		}
	}
	
	
    //This method is for Add New Policy with Application and Permission
	@Transactional
	public ResponseEntity<String> createPolicyWithAppAndPermissions(Map<String, Object> requestData) {
	    // Extract policy details from the request data
	    Map<String, Object> policyData = (Map<String, Object>) requestData.get("policy");
	    String policyName = (String) policyData.get("name");
	    String policyDescription = (String) policyData.get("description");

	    // Check for missing data in the request
	    if (policyName == null || policyDescription == null) {
	        return ResponseEntity.badRequest().body("Missing 'name' or 'description' in policy data");
	    }

	    // Create a new policy
	    Policy policy = new Policy();
	    policy.setPolicyName(policyName);
	    policy.setPolicyDescription(policyDescription);
	    policy.setPermissions(new ArrayList<>()); // Initialize permissions list

	    // Fetch existing applications based on the application data
	    List<Application> applications = new ArrayList<>();
	    if (policyData.containsKey("applications")) {
	        List<Map<String, Object>> applicationDataList = (List<Map<String, Object>>) policyData.get("applications");
	        for (Map<String, Object> applicationData : applicationDataList) {
	            String applicationName = (String) applicationData.get("name");
	            List<String> permissionNames = (List<String>) applicationData.get("permissions");

	            // Fetch existing application from the repository based on its name
	            Optional<Application> optionalApplication = applicationRepository.findByAppName1(applicationName);
	            Application application = optionalApplication.orElseThrow(() -> new IllegalArgumentException("Application not found: " + applicationName));

	            // Fetch existing permissions and associate them with the application
	            List<Permission> permissions = permissionRepository.findByPermissionNameIn(permissionNames);
	            if (permissions.size() != permissionNames.size()) {
	                throw new IllegalArgumentException("Not all permissions found for application: " + applicationName);
	            }

	            application.setPermissions(permissions);
	            applications.add(application);
	        }
	    }

	    // Set applications for the policy and track assigned permissions
	    policy.setApplications(applications);
	    List<Permission> assignedPermissions = new ArrayList<>();  // Track assigned permissions

	    List<String> errorMessages = new ArrayList<>();

	    for (Application application : applications) {
	        List<Permission> applicationPermissions = application.getPermissions();

	        // Check for duplicate permissions within the application
	        if (applicationPermissions.stream().anyMatch(p -> assignedPermissions.contains(p))) {
	            errorMessages.add("Duplicate permissions found within application '" + application.getAppName() + "'");
	            continue;  // Skip to the next application
	        }

	        // Check if any permissions are already assigned to the policy
	        for (Permission permission : applicationPermissions) {
	            if (policy.getPermissions().contains(permission)) {
	                errorMessages.add("Permission '" + permission.getPermissionName() + "' already assigned to policy");
	            } else {
	                // If the permission is not already assigned, fetch it from the repository
	                Optional<Permission> optionalPermission = permissionRepository.findByPermissionName1(permission.getPermissionName());
	                Permission fetchedPermission = optionalPermission.orElseThrow(() -> new IllegalArgumentException("Permission not found: " + permission.getPermissionName()));

	                // Add the fetched permission to the policy
	                policy.getPermissions().add(fetchedPermission);
	                assignedPermissions.add(fetchedPermission);  // Track assigned permissions
	            }
	        }
	    }

	    // Check if there are any error messages
	    if (!errorMessages.isEmpty()) {
	        // If there are errors, return a bad request response with error messages
	        return ResponseEntity.badRequest().body(String.join(", ", errorMessages));
	    }

	    // Save the policy and return a success response
	    policyRepository.save(policy);
	    return ResponseEntity.ok("Policy with Applications and Permissions created successfully");
	}

	
	
	
	//This method is for Assign Policies to User
	 @Transactional
	 public ResponseEntity<String> assignPoliciesToUser(Map<String, Object> requestData) {
	     // Extract userName and policies from the request data
	     String userName = (String) requestData.get("userName");
	     List<Map<String, Object>> policiesData = (List<Map<String, Object>>) requestData.get("policies");

	     // Check for missing data in the request
	     if (userName == null || policiesData == null) {
	         return ResponseEntity.badRequest().body("Missing 'userName' or 'policies' in request data");
	     }

	     // Fetch the user from the repository based on userName
	     User user = userRepository.findByname(userName);

	     // Check if the user exists and is active
	     if (user != null && user.isAccountStatus()) {
	         List<String> errorMessages = new ArrayList<>();

	         // Loop through each policy in policiesData
	         for (Map<String, Object> policyData : policiesData) {
	             // Extract policy details from the data
	             String policyName = (String) policyData.get("policyName");
	             // Add additional policy details extraction based on your needs

	             // Fetch the policy from the repository based on policyName
	             Policy policy = policyRepository.findByPolicyName(policyName);

	             // Check if the policy exists
	             if (policy != null) {
	                 // If the policy exists, associate it with the user
	                 List<Policy> userPolicies = user.getPolicies();

	                 // Initialize userPolicies if null
	                 if (userPolicies == null) {
	                     userPolicies = new ArrayList<>();
	                 }

	                 // Check if the user already has the policy
	                 if (userPolicies.stream().noneMatch(p -> p.getPolicyName().equals(policyName))) {
	                     // If the policy is not already assigned, add it
	                     userPolicies.add(policy);
	                     user.setPolicies(userPolicies);
	                 } else {
	                     // Store error message for the user
	                     errorMessages.add("Policy '" + policyName + "' already assigned to user '" + userName + "'");
	                 }
	             } else {
	                 // Store error message for the user
	                 errorMessages.add("Policy '" + policyName + "' not found");
	             }
	         }

	         // Save the user with updated policies only if there are no errors
	         if (errorMessages.isEmpty()) {
	             userRepository.save(user);
	             return ResponseEntity.ok("Policies assigned successfully");
	         } else {
	             // Return error response if there are error messages
	             return ResponseEntity.badRequest().body(errorMessages.toString());
	         }
	     } else {
	         // Log or handle inactive users as needed or return appropriate error response
	         return ResponseEntity.badRequest().body("User not found or inactive");
	     }
	 }

	 
	 
	//This method is for Remove Policies from User
	 @Transactional
	 public ResponseEntity<String> removePoliciesFromUser(Map<String, Object> requestData) {
	     // Check for missing data
	     String userName = requestData.get("name").toString();
	     List<String> policyNames = (List<String>) requestData.get("policyNames");

	     if (userName == null || policyNames == null || policyNames.isEmpty()) {
	         return ResponseEntity.badRequest().body("Missing 'userName' or 'policyNames'");
	     }

	     // Fetch the user from the repository based on userName
	     User user = userRepository.findByname(userName);

	     // Check if the user exists and is active
	     if (user != null && user.isAccountStatus()) {
	         List<String> removedPolicies = new ArrayList<>();

	         // Loop through each policy name to be removed
	         for (String policyName : policyNames) {
	             // Fetch the policy from the repository based on policyName
	             Policy policy = policyRepository.findByPolicyName(policyName);

	             // Check if the policy exists
	             if (policy != null) {
	                 // Remove the policy from the user's policies
	                 List<Policy> userPolicies = user.getPolicies();

	                 // Check if the user has the policy
	                 if (userPolicies != null && userPolicies.removeIf(p -> p.getPolicyName().equals(policyName))) {
	                     removedPolicies.add(policyName);
	                 }
	             }
	         }

	         // Save the user with updated policies
	         userRepository.save(user);

	         if (!removedPolicies.isEmpty()) {
	             return ResponseEntity.ok("Policies " + removedPolicies + " removed from user '" + userName + "'");
	         } else {
	             return ResponseEntity.badRequest().body("No valid policies found to remove from user '" + userName + "'");
	         }
	     } else {
	         // Log or handle inactive users as needed or return appropriate error response
	         return ResponseEntity.badRequest().body("User not found or inactive");
	     }
	 }

	 
	 
	 
	 //This method is for Assign Policies to Role
		@Transactional
		public ResponseEntity<String> assignPoliciesToRole(Map<String, Object> requestData) {
		    // Extract roleName and policies from the request data
		    String roleName = (String) requestData.get("roleName");
		    List<Map<String, Object>> policiesData = (List<Map<String, Object>>) requestData.get("policies");

		    // Check for missing data in the request
		    if (roleName == null || policiesData == null) {
		        return ResponseEntity.badRequest().body("Missing 'roleName' or 'policies' in request data");
		    }

		    // Fetch the role from the repository based on roleName
		    Role role = roleRepository.findByRoleName1(roleName);

		    // Check if the role exists
		    if (role != null) {
		        List<String> errorMessages = new ArrayList<>();

		        // Loop through each policy in policiesData
		        for (Map<String, Object> policyData : policiesData) {
		            // Extract policy details from the data
		            String policyName = (String) policyData.get("policyName");

		            // Fetch the policy from the repository based on policyName
		            Policy policy = policyRepository.findByPolicyName(policyName);

		            // Check if the policy exists
		            if (policy != null) {
		                // If the policy exists, associate it with the role
		                List<Policy> rolePolicies = role.getPolicies();

		                // Initialize rolePolicies if null
		                if (rolePolicies == null) {
		                    rolePolicies = new ArrayList<>();
		                }

		                // Check if the role already has the policy
		                if (rolePolicies.stream().noneMatch(p -> p.getPolicyName().equals(policyName))) {
		                    // If the policy is not already assigned, add it
		                    rolePolicies.add(policy);
		                    role.setPolicies(rolePolicies);
		                } else {
		                    // Store error message for the role
		                    errorMessages.add("Policy '" + policyName + "' already assigned to role '" + roleName + "'");
		                }
		            } else {
		                // Store error message for the role
		                errorMessages.add("Policy '" + policyName + "' not found");
		            }
		        }

		        // Save the role with updated policies only if there are no errors
		        if (errorMessages.isEmpty()) {
		            roleRepository.save(role);
		            return ResponseEntity.ok("Policies assigned successfully to role");
		        } else {
		            // Return error response if there are error messages
		            return ResponseEntity.badRequest().body(errorMessages.toString());
		        }
		    } else {
		        // Return appropriate error response if the role is not found
		        return ResponseEntity.badRequest().body("Role not found");
		    }
		}
		
		
		
		//This method is for Remove Policies from Role
		@Transactional
		public ResponseEntity<String> removePoliciesFromRole(Map<String, Object> requestData) {
		    // Check for missing data
		    String roleName = requestData.get("roleName").toString();
		    List<String> policyName = (List<String>) requestData.get("policyName");

		    if (roleName == null || policyName == null || policyName.isEmpty()) {
		        return ResponseEntity.badRequest().body("Missing 'roleName' or 'policyNames'");
		    }

		    // Fetch the role from the repository based on roleName
		    Role role = roleRepository.findByRoleName1(roleName);

		    // Check if the role exists
		    if (role != null) {
		        List<String> removedPolicies = new ArrayList<>();

		        // Loop through each policy name to be removed
		        for (String policyName1 : policyName) {
		            // Fetch the policy from the repository based on policyName
		            Policy policy = policyRepository.findByPolicyName(policyName1);

		            // Check if the policy exists
		            if (policy != null) {
		                // Remove the policy from the role's policies
		                List<Policy> rolePolicies = role.getPolicies();

		                // Check if the role has the policy
		                if (rolePolicies != null && rolePolicies.removeIf(p -> p.getPolicyName().equals(policyName1))) {
		                    removedPolicies.add(policyName1);
		                }
		            }
		        }

		        // Save the role with updated policies
		        roleRepository.save(role);

		        if (!removedPolicies.isEmpty()) {
		            return ResponseEntity.ok("Policies " + removedPolicies + " removed from role '" + roleName + "'");
		        } else {
		            return ResponseEntity.badRequest().body("No valid policies found to remove from role '" + roleName + "'");
		        }
		    } else {
		        // Return appropriate error response if the role is not found
		        return ResponseEntity.badRequest().body("Role not found");
		    }
		}
		
		
		
		//This method is for Assign Policies to Group
		 @Transactional
		 public ResponseEntity<String> assignPoliciesToGroup(Map<String, Object> requestData) {
		     // Extract groupName and policies from the request data
		     String groupName = (String) requestData.get("groupName");
		     List<Map<String, Object>> policiesData = (List<Map<String, Object>>) requestData.get("policies");

		     // Check for missing data in the request
		     if (groupName == null || policiesData == null) {
		         return ResponseEntity.badRequest().body("Missing 'groupName' or 'policies' in request data");
		     }

		     // Fetch the group from the repository based on groupName
		     Group group = groupRepository.findByGroupName1(groupName);

		     // Check if the group exists
		     if (group != null) {
		         List<String> errorMessages = new ArrayList<>();

		         // Loop through each policy in policiesData
		         for (Map<String, Object> policyData : policiesData) {
		             // Extract policy details from the data
		             String policyName = (String) policyData.get("policyName");
		             // Add additional policy details extraction based on your needs

		             // Fetch the policy from the repository based on policyName
		             Policy policy = policyRepository.findByPolicyName(policyName);

		             // Check if the policy exists
		             if (policy != null) {
		                 // If the policy exists, associate it with the group
		                 List<Policy> groupPolicies = group.getPolicies();

		                 // Initialize groupPolicies if null
		                 if (groupPolicies == null) {
		                     groupPolicies = new ArrayList<>();
		                 }

		                 // Check if the group already has the policy
		                 if (groupPolicies.stream().noneMatch(p -> p.getPolicyName().equals(policyName))) {
		                     // If the policy is not already assigned, add it
		                     groupPolicies.add(policy);
		                     group.setPolicies(groupPolicies);
		                 } else {
		                     // Store error message for the group
		                     errorMessages.add("Policy '" + policyName + "' already assigned to group '" + groupName + "'");
		                 }
		             } else {
		                 // Store error message for the group
		                 errorMessages.add("Policy '" + policyName + "' not found");
		             }
		         }

		         // Save the group with updated policies only if there are no errors
		         if (errorMessages.isEmpty()) {
		             groupRepository.save(group);
		             return ResponseEntity.ok("Policies assigned to group successfully");
		         } else {
		             // Return error response if there are error messages
		             return ResponseEntity.badRequest().body(errorMessages.toString());
		         }
		     } else {
		         // Handle the case where the group is not found
		         return ResponseEntity.badRequest().body("Group not found");
		     }
		 }
	
		 
		//This method is for Remove Policies from Group
		 @Transactional
		 public ResponseEntity<String> removePoliciesFromGroup(Map<String, Object> requestData) {
		     // Check for missing data
		     String groupName = requestData.get("groupName").toString();
		     List<String> policyNamesToRemove = (List<String>) requestData.get("policyName");

		     if (groupName == null || policyNamesToRemove == null || policyNamesToRemove.isEmpty()) {
		         return ResponseEntity.badRequest().body("Missing 'groupName' or 'policyName'");
		     }

		     // Fetch the group from the repository based on groupName
		     Group group = groupRepository.findByGroupName1(groupName);

		     // Check if the group exists
		     if (group != null) {
		         List<String> removedPolicies = new ArrayList<>();

		         // Loop through each policy name to be removed
		         for (String policyName : policyNamesToRemove) {
		             // Fetch the policy from the repository based on policyName
		             Policy policy = policyRepository.findByPolicyName(policyName);

		             // Check if the policy exists
		             if (policy != null) {
		                 // Remove the policy from the group's policies
		                 List<Policy> groupPolicies = group.getPolicies();

		                 // Check if the group has the policy
		                 if (groupPolicies != null && groupPolicies.removeIf(p -> p.getPolicyName().equals(policyName))) {
		                     removedPolicies.add(policyName);
		                 }
		             }
		         }
	 
		         // Save the group with updated policies
		         groupRepository.save(group);

		         if (!removedPolicies.isEmpty()) {
		             return ResponseEntity.ok("Policies " + removedPolicies + " removed from group '" + groupName + "'");
		         } else {
		             return ResponseEntity.badRequest().body("No valid policies found to remove from group '" + groupName + "'");
		         }
		     } else {
		         // Return appropriate error response if the group is not found
		         return ResponseEntity.badRequest().body("Group not found");
		     }
		 }



	 
}
