package com.ik.iam.services;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.ik.iam.emailService.EmailService;
import com.ik.iam.model.User;
import com.ik.iam.repository.UserRepository;
import com.ik.iam.sequnceGenerator.SequenceGeneratorService;

@Service
public class AuthenticationServices  {

	
@Autowired 
private  SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private UserRepository userRepository;
    
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private EmailService emailService; 
    
    //This method is for Register New User
    @Transactional
    public void registerUser(Map<String, Object> requestData) {
        // Extract user information from request data
        String name = (String) requestData.get("name");
        String email = (String) requestData.get("email");
        String password = (String) requestData.get("password");

        // Encode the raw password using BCrypt
        String encodedPassword = passwordEncoder.encode(password);

        // Check if user with email already exists
        Optional<User> existingUserOptional = userRepository.findByEmail(email);

        if (existingUserOptional.isPresent()) {
            // User already exists
            User existingUser = existingUserOptional.get();
            if (!existingUser.isAccountStatus()) {

                // User exists but is not active, update verification token and send email
                String newVerificationToken = generateVerificationToken();
                existingUser.setVerificationToken(newVerificationToken);
                existingUser.setExpiryAt(LocalDateTime.now().plusSeconds(120));
                existingUser.setConfirmedAt(null);
                existingUser.setVerified(false);
                existingUser.setPassword(encodedPassword); 

                // Save updated user and send verification email
                userRepository.save(existingUser);
                emailService.sendVerificationEmail(existingUser.getEmail(), newVerificationToken, "Account Verification");
            } else {
                // User exists and is already active
                throw new RuntimeException("Email already exists and account is active.");
            }
        } else {
            // User does not exist, create a new user
        	
            String verificationToken = generateVerificationToken();
            User newUser = new User();
            long UserId = sequenceGeneratorService.generateSequence("User_Id");
            newUser.setUserId(UserId);
            newUser.setName(name);
            
            newUser.setEmail(email);
            newUser.setPassword(encodedPassword);
            newUser.setVerificationToken(verificationToken);
            newUser.setCreatedAt(LocalDateTime.now());
            newUser.setExpiryAt(LocalDateTime.now().plusSeconds(600));
            newUser.setConfirmedAt(null);
            newUser.setVerified(false);

            // Save user and send verification email
            userRepository.insert(newUser);
            emailService.sendVerificationEmail(newUser.getEmail(), verificationToken, "Account Verification");
        }
    }



    

    // This method is for generating random Token
    private String generateVerificationToken() {
        // Implement your logic for generating a random token
        // For example, using UUID or other secure methods
        return java.util.UUID.randomUUID().toString();
    }

    
    //This method is for Verifying the Register User Account
    @Transactional
    public ResponseEntity<String> verifyRegistrationAcc(String token) {
        Optional<User> optionalUser = userRepository.findByVerificationToken(token);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            LocalDateTime currentTime = LocalDateTime.now();

            if (!user.isVerified()) {
                if (currentTime.isBefore(user.getExpiryAt())) {
                    // Mark the user as verified
                    user.setVerified(true);
                    user.setConfirmedAt(LocalDateTime.now());

                    // If AccountStatus is false, set it to true
                    if (!user.isAccountStatus()) {
                        user.setAccountStatus(true);
                    }

                    userRepository.save(user);

                    return ResponseEntity.ok(" Your Account has been verified successfully!, Now yOu Are Ready to login.");
                } else {
                    // Token has expired, generate a new token and send it to the user
                    String newVerificationToken = generateVerificationToken();
                    user.setVerificationToken(newVerificationToken);
                    user.setExpiryAt(LocalDateTime.now().plusSeconds(120));
                    userRepository.save(user);
                    emailService.sendVerificationEmail(user.getEmail(), newVerificationToken, "Account Verification");

                    return ResponseEntity.badRequest().body("Verification token expired. A new token has been sent to your email.");
                }
            } else {
                return ResponseEntity.badRequest().body("Account already verified. Login With your Credentials.");
            }
        } else {
            return ResponseEntity.badRequest().body("User not found for the given verification token.");
        }
    }
    
//  This Method is for Login  
    @Transactional
    public ResponseEntity<String> loginUser(Map<String, Object> requestData) {
        String email = (String) requestData.get("email");
        String password = (String) requestData.get("password");

        Optional<User> optionalUser = userRepository.findByEmail(email);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            if (user.isLoggedIn()) {
                return ResponseEntity.badRequest().body("You are already logged in.");
            }

            // Check if the stored password needs to be upgraded to BCrypt
            if (!passwordEncoder.upgradeEncoding(user.getPassword())) {
                // If not, encode the entered password with BCrypt for comparison
                String encodedPassword = passwordEncoder.encode(password);

                // Check if the entered password matches the stored BCrypt-encoded password
                if (passwordEncoder.matches(password, user.getPassword())) {
                    // Check if the user is verified and the account is active
                    if (user.isVerified() && user.isAccountStatus()) {
                        // Set the isLoggedIn attribute to true and save the changes
                        user.setLoggedIn(true);
                        userRepository.save(user);

                        return ResponseEntity.ok("Login successful!");
                    } else {
                        return ResponseEntity.badRequest().body("User account not verified or inactive.");
                    }
                } else {
                    return ResponseEntity.badRequest().body("Incorrect password.");
                }
            } else {
                return ResponseEntity.badRequest().body("User not found.");
            }
        } else {
            return ResponseEntity.badRequest().body("User not found.");
        }
    }

    
    private String generateConfirmationToken() {
        // Implement your logic for generating a random token
        // For example, using UUID or other secure methods
        return UUID.randomUUID().toString();
    }


    //This method is for Logout User
    @Transactional
    public ResponseEntity<String> logoutUser(Map<String, Object> requestData) {
    	String email=(String) requestData.get("email");
    	
        Optional<User> optionalUser = userRepository.findByEmail(email);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            // Check the isLoggedIn attribute
            if (user.isLoggedIn()) {
                // Set the isLoggedIn attribute to false and save the changes
                user.setLoggedIn(false);
                userRepository.save(user);

                return ResponseEntity.ok("Logout successful!");
            } else {
                return ResponseEntity.badRequest().body("User has already logged out.");
            }
        } else {
            return ResponseEntity.badRequest().body("User not found with This Email. Please Enter your Email.");
        }
    }
	
    // This method is to invite user 
   	@Transactional
       public void sendInvitation(Map<String, Object> requestData) {
   		String email=(String) requestData.get("email");
           
           // Create user with false status and store in database
           User user = new User();
           user.setEmail(email);
           user.setAccountStatus(false);
           user = userRepository.save(user);

           // Send invitation email
           emailService.sendInvitationEmail(email);
       }
   	
   	
   	//This method is for Register Invited user
   	@Transactional
	public String registerInvitedUser(Map<String, Object> requestData) {
	    // Extract user information from request data
	    String name = (String) requestData.get("name");
	    String email = (String) requestData.get("email");
	    String password = (String) requestData.get("password");

	    // Encode the raw password using BCrypt
	    String encodedPassword = passwordEncoder.encode(password);

	    // Check if user with email already exists
	    Optional<User> existingUserOptional = userRepository.findByEmail(email);

	    if (existingUserOptional.isPresent()) {
	        // User already exists
	        User existingUser = existingUserOptional.get();
	        if (existingUser.isAccountStatus()) {
	            // User exists and is already active
	            throw new RuntimeException("Email already exists, and the account is active.");
	        } else {
	            // User exists but is not active, update password and set account status to true
	            existingUser.setPassword(encodedPassword);
	            existingUser.setAccountStatus(true);
	            userRepository.save(existingUser);
	            return "You have been successfully registered, and your account is active.";
	        }
	    } else {
	        // User does not exist, create a new user with account status true
	        User newUser = new User();
	        long userId = sequenceGeneratorService.generateSequence("User_Id");
	        newUser.setUserId(userId);
	        newUser.setName(name);
	        newUser.setEmail(email);
	        newUser.setPassword(encodedPassword);
	        newUser.setCreatedAt(LocalDateTime.now());
	        newUser.setAccountStatus(true);  // Set account status to true

	        // Save user
	        userRepository.insert(newUser);
	        return "You have been successfully registered, and your account is active.";
	    }
	}
    
    
    
}
