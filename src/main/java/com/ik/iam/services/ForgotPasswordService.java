package com.ik.iam.services;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import com.ik.iam.emailService.EmailService;
import com.ik.iam.model.User;
import com.ik.iam.repository.UserRepository;

import ch.qos.logback.classic.Logger;

@Service
public class ForgotPasswordService {

	
	

	private static final Logger logger = (Logger) LoggerFactory.getLogger(ForgotPasswordService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private EmailService emailService;
    
    
    //This Method is for Requesting the Password Reset
    public ResponseEntity<String> requestPasswordReset(Map<String, Object> requestData) {
        String email = (String) requestData.get("email");

        if (email == null || email.isBlank()) {
            return ResponseEntity.badRequest().build(); // Missing email address
        }

        java.util.Optional<User> userOptional = userRepository.findByEmail(email);
        if (!userOptional.isPresent()) {
            return ResponseEntity.badRequest().build(); // User not found
        }

        User user = userOptional.get();
        String token = generateRandomToken();
        LocalDateTime expiryAt = LocalDateTime.now().plusSeconds(500);

        user.setResetPasswordToken(token);
        user.setResetPasswordTokenCreatedAt(LocalDateTime.now());
        user.setResetPasswordTokenExpiryAt(expiryAt);
        userRepository.save(user);

        emailService.sendPasswordResetEmail(user.getEmail(), token);

        return ResponseEntity.ok().build().ok("Password reset link sent to the email address");
    }

    private String generateRandomToken() {
        return UUID.randomUUID().toString();
    }
   

//   This method is for Verify Password Reset Token
    public ResponseEntity<String> verifyPasswordResetToken(String resetPasswordToken) {
        try {
            // Find user by reset password token
            User user = userRepository.findByResetPasswordToken(resetPasswordToken)
                    .orElseThrow(() -> new RuntimeException("Invalid token"));

            // Check if the token has expired
            if (LocalDateTime.now().isAfter(user.getResetPasswordTokenExpiryAt())) {
                return ResponseEntity.badRequest().body("Token expired");
            }

            // Token is valid, you can return a success response with a message
            return ResponseEntity.ok().body("Set your new password!");
        } catch (RuntimeException e) {
            // Handle invalid token exception
            return ResponseEntity.badRequest().body("Invalid token");
        }
    }

  
    //This method is for Setting New Password
    public ResponseEntity<String> setNewPassword(Map<String, Object> requestData) {
        String newPassword = (String) requestData.get("password");

        try {
            // Retrieve the user based on the reset token
            java.util.Optional<User> validUserOptional = userRepository.findByResetPasswordToken(requestData.get("token"))
                    .map(user -> {
                        if (LocalDateTime.now().isAfter(user.getResetPasswordTokenExpiryAt())) {
                            logger.warn("Password reset token has expired for user: {}", user.getEmail());
                            throw new RuntimeException("Token expired");
                        }
                        return user;
                    }).map(user -> {
                        // Add your password validation logic here if needed
                        String hashedPassword = passwordEncoder.encode(newPassword);
                        user.setPassword(hashedPassword);
                        user.setResetPasswordToken(null);
                        user.setResetPasswordTokenExpiryAt(null);
                        return user;
                    });

            User user = validUserOptional.orElseThrow(() -> {
                logger.warn("Invalid password reset token");
                return new RuntimeException("Invalid token");
            });

            userRepository.save(user);

            logger.info("Password successfully reset for user: {}", user.getEmail());

            return ResponseEntity.ok().build().ok("Password successfully reset.");

        } catch (Exception e) {
            logger.error("Error setting new password", e);
            return ResponseEntity.badRequest().body("Error setting new password: " + e.getMessage());
        }
    }

    
//   
    private boolean isTokenValid(User user) {
        LocalDateTime expiryAt = user.getExpiryAt();
        return expiryAt != null && LocalDateTime.now().isBefore(expiryAt);
    }
    
    

}
