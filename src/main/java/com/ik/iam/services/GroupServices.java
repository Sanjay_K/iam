package com.ik.iam.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ik.iam.model.Group;
import com.ik.iam.repository.GroupRepository;
import com.ik.iam.sequnceGenerator.SequenceGeneratorService;

@Service
public class GroupServices  {

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;

	//This method is for Add Group
	public void addGroup(Map<String, Object> requestData) {
		// Extract group registration data from the requestData map
		String groupName = (String) requestData.get("groupName");
		String groupDescription = (String) requestData.get("groupDescription");

		// Apply validation rules for each attribute
//		    validateAttribute(groupName, "groupName");

		// Check if the group name is already taken
		if (groupRepository.existsByGroupName(groupName)) {
			throw new IllegalArgumentException("Group name is already taken!");
		}

		// Generate group ID
		long groupId = sequenceGeneratorService.generateSequence("Group_Id");
		System.out.println("Generated Group Id: " + groupId);

		// Create a new group entity and save it
		Group newGroup = new Group();
		newGroup.setGroupId(groupId);
		newGroup.setGroupName(groupName);
		newGroup.setGroupDescription(groupDescription);
		groupRepository.save(newGroup);
	}


	//This method is for get Group By Id
	public ResponseEntity<Object> getGroupById(Long groupId) {
	    try {
	        Optional<Group> optionalGroup = groupRepository.findByGroup_Id(groupId);

	        if (optionalGroup.isPresent()) {
	            Group group = optionalGroup.get();

	            Map<String, Object> groupMap = new HashMap<>();
	            groupMap.put("groupId", group.getGroupId());
	            groupMap.put("groupName", group.getGroupName());
	            groupMap.put("groupDescription", group.getGroupDescription());
	            // Add other fields as needed

	            return ResponseEntity.ok(groupMap);
	        } else {
	            System.out.println("No group found for ID: " + groupId);
	            // Handle the case where no group is found for the given ID
	            Map<String, String> responseMap = Collections.singletonMap("message",
	                    "No group found for ID: " + groupId);
	            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
	        }
	    } catch (Exception e) {
	        // Log any exceptions for debugging
	        e.printStackTrace();
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                .body(Collections.singletonMap("error", e.getMessage()));
	    }
	}


	//This method is for Get Group by Name
	public ResponseEntity<Object> getGroupByName(String groupName) {
        try {
            List<Group> groupsList = groupRepository.findByGroupNameIgnoreCase(groupName.trim());

            if (!groupsList.isEmpty()) {
                List<Map<String, Object>> groupList = groupsList.stream().map(group -> {
                    Map<String, Object> groupMap = new HashMap<>();
                    groupMap.put("groupId", group.getGroupId());
                    groupMap.put("groupName", group.getGroupName());
                    groupMap.put("groupDescription", group.getGroupDescription());
                    // Add other fields as needed
                    return groupMap;
                }).collect(Collectors.toList());

                return ResponseEntity.ok(groupList);
            } else {
                System.out.println("No groups found for name: " + groupName);
                // Handle the case where no groups are found
                Map<String, String> responseMap = Collections.singletonMap("message",
                        "No groups found for name: " + groupName);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
            }
        } catch (Exception e) {
            // Log any exceptions for debugging
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonMap("error", e.getMessage()));
        }
    }
	
	//This method is for Update Group By Name
    @Transactional
	public Group updateGroupByName(String groupName, Map<String, Object> requestData) throws NotFoundException {
		// 1. Check if the group exists by name
		Group existingGroup = groupRepository.findByGroupName1(groupName);
		if (existingGroup == null) {
			throw new NotFoundException();
		}

		// 2. Validate and apply updates from request data
		for (Map.Entry<String, Object> entry : requestData.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();

			// Check only specific attributes to update
			switch (key) {
			case "groupDescription":
				if (value instanceof String) {
					existingGroup.setGroupDescription((String) value);
				} else {
					// Handle validation or throw an exception for invalid data type
					throw new IllegalArgumentException("Invalid data type for groupDescription");
				}
				break;
			
			default:
				// Ignore other attributes or throw an exception for unknown attributes
				throw new IllegalArgumentException("Unknown attribute: " + key);
			}
		}

		// 3. Save the updated group to MongoDB
		return groupRepository.save(existingGroup);
	}

    //This Method is for Delete Group
	public String deleteGroupBygroupName(String groupName) {
		groupRepository.deleteBygroupName(groupName);
		return "Deleted This Group Successfully";
	}

	
	//This method is for GetAll Groups
	public ResponseEntity<Object> getAllGroups() {
		try {
			List<Group> allGroups = groupRepository.findAll();

			if (!allGroups.isEmpty()) {
				List<Map<String, Object>> groupList = allGroups.stream().map(group -> {
					Map<String, Object> groupMap = new HashMap<>();
					groupMap.put("groupId", group.getGroupId());

					// Check for null values before adding to the map
					if (group.getGroupName() != null) {
						groupMap.put("groupName", group.getGroupName());
					}

					if (group.getGroupDescription() != null) {
						groupMap.put("groupDescription", group.getGroupDescription());
					}

					// Add other fields as needed

					return groupMap;
				}).collect(Collectors.toList());

				// Log the response body
				// System.out.println("Response Body: " + groupList);

				return ResponseEntity.ok(groupList);
			} else {
				System.out.println("No groups found.");
				// Handle the case where no groups are found
				Map<String, String> responseMap = Collections.singletonMap("message", "No groups found.");
				// System.out.println("Response Body: " + responseMap);
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
			}
		} catch (Exception e) {
			// Log any exceptions for debugging
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(Collections.singletonMap("error", e.getMessage()));
		}
	}

}
