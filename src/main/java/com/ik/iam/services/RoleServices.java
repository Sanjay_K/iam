package com.ik.iam.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import com.ik.iam.model.Role;
import com.ik.iam.repository.RoleRepository;
import com.ik.iam.sequnceGenerator.SequenceGeneratorService;

@Service
public class RoleServices {
	
	 @Autowired
	    private RoleRepository roleRepository;
	     
		@Autowired
		private SequenceGeneratorService sequenceGeneratorService;

		
		//This method is for Add Role
	    public ResponseEntity<Object> addRole(Map<String, Object> requestData) {
	        try {
	            // Extract role registration data from the requestData map
	        	long roleId = sequenceGeneratorService.generateSequence("Role_Id");
	            String roleName = (String) requestData.get("roleName");
	            String roleDescription = (String) requestData.get("roleDescription");

		
	            // Check if the role with the same roleId is already registered
	            if (roleRepository.existsByRoleId(roleId)) {
	                throw new IllegalArgumentException("Role with roleId " + roleId + " already registered!");
	            }

	            // Generate role ID (if needed)
//	             long generatedRoleId = sequenceGeneratorService.generateSequence("Role_Id", 100);
//	            System.out.println("Generated Role Id: " + generatedRoleId);

	            // Create a new role entity and save it
	            Role newRole = new Role();
	            newRole.setRoleId(roleId);
	            newRole.setRoleName(roleName);
	            newRole.setRoleDescription(roleDescription);
	            Role savedRole = roleRepository.save(newRole);



	            return ResponseEntity.status(HttpStatus.CREATED).body(savedRole);
	        } catch (Exception e) {
	            // Log any exceptions for debugging
	            e.printStackTrace();
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(Collections.singletonMap("error", e.getMessage()));
	        }
	    }

	    //This method is to  Get Role By Name
	    public ResponseEntity<Object> getRoleByName(String roleName) {
	        try {
	            List<Role> rolesList = roleRepository.findByRoleNameIgnoreCase(roleName.trim());

	            if (!rolesList.isEmpty()) {
	                List<Map<String, Object>> roleList = rolesList.stream().map(role -> {
	                    Map<String, Object> roleMap = new HashMap<>();
	                    roleMap.put("roleId", role.getRoleId());
	                    roleMap.put("roleName", role.getRoleName());
	                    roleMap.put("roleDescription", role.getRoleDescription());
	                    // Add other fields as needed
	                    return roleMap;
	                }).collect(Collectors.toList());

	                return ResponseEntity.ok(roleList);
	            } else {
	                System.out.println("No roles found for roleName: " + roleName);
	                // Handle the case where no roles are found
	                Map<String, String> responseMap = Collections.singletonMap("message",
	                        "No roles found for roleName: " + roleName);
	                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	            }
	        } catch (Exception e) {
	            // Log any exceptions for debugging
	            e.printStackTrace();
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(Collections.singletonMap("error", e.getMessage()));
	        }
	    }
	    
	    
	    //This method is to GetAll Roles
	    public ResponseEntity<Object> getAllRoles() {
	        try {
	            List<Role> allRoles = roleRepository.findAll();

	            if (!allRoles.isEmpty()) {
	                List<Map<String, Object>> roleList = allRoles.stream().map(role -> {
	                    Map<String, Object> roleMap = new HashMap<>();
	                    roleMap.put("roleId", role.getRoleId());

	                    // Check for null values before adding to the map
	                    if (role.getRoleName() != null) {
	                        roleMap.put("roleName", role.getRoleName());
	                    }

	                    if (role.getRoleDescription() != null) {
	                        roleMap.put("roleDescription", role.getRoleDescription());
	                    }

	                    // Add other fields as needed
	                    return roleMap;
	                }).collect(Collectors.toList());

	                // Log the response body
	                System.out.println("Response Body: " + roleList);

	                return ResponseEntity.ok(roleList);
	            } else {
	                System.out.println("No roles found.");
	                // Handle the case where no roles are found
	                Map<String, String> responseMap = Collections.singletonMap("message", "No roles found.");
	                System.out.println("Response Body: " + responseMap);
	                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
	            }
	        } catch (Exception e) {
	            // Log any exceptions for debugging
	            e.printStackTrace();
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(Collections.singletonMap("error", e.getMessage()));
	        }
	    }

	    //This method is to Update Role By Name
	    public Object updateRoleByName(String roleName, Map<String, Object> requestData) {
	        try {
	            // Find the existing role by name
	            Optional<Role> optionalRole = roleRepository.findByRoleName(roleName);

	            // Check if the role exists
	            if (optionalRole.isPresent()) {
	                Role existingRole = optionalRole.get();

	                // Update the role details with the provided values in the requestData map
	                if (requestData.containsKey("roleName")) {
	                    existingRole.setRoleName((String) requestData.get("roleName"));
	                }

	                if (requestData.containsKey("roleDescription")) {
	                    existingRole.setRoleDescription((String) requestData.get("roleDescription"));
	                }

	                // Update other fields as needed

	                // Save the updated role
	                roleRepository.save(existingRole);

	                // Return only the updated role in the response body
	                return existingRole;
	            } else {
	                // Return a not found response with an empty body
	                return ResponseEntity.notFound().build();
	            }
	        } catch (Exception e) {
	            // Log any exceptions for debugging
	            e.printStackTrace();
	            // Return an internal server error response with an error message in the body
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(Collections.singletonMap("error", e.getMessage()));
	        }
	    }

	  //This method is for Delete Role
	    public String deleteRoleByRoleName(String roleName) {
	        try {
	            // Find the role by roleName
	            Role roleToDelete = roleRepository.findByRoleName1(roleName);

	            // Check if the role exists
	            if (roleToDelete != null) {
	                // Delete the role
	            	roleRepository.delete(roleToDelete);
	                return "Deleted Role Successfully";
	            } else {
	                // Return a message indicating that the role was not found
	                return "Role with the specified name not found";
	            }
	        } catch (Exception e) {
	            // Log any exceptions for debugging
	            e.printStackTrace();
	            // Return an error message
	            return "Error deleting role: " + e.getMessage();
	        }
	    }

	 
	
	
}
