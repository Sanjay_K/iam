package com.ik.iam.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ik.iam.model.Group;
import com.ik.iam.model.Permission;
import com.ik.iam.model.Policy;
import com.ik.iam.model.Role;
import com.ik.iam.repository.PermissionRepository;
import com.ik.iam.sequnceGenerator.SequenceGeneratorService;



@Service
public class PermissionService {
	
	@Autowired
    private  PermissionRepository permissionRepository;
    
    @Autowired
   	private SequenceGeneratorService sequenceGeneratorService;

 
     //This method is for Add Permission
    public void addPermission(Map<String, Object> requestData) { {
		// Extract group registration data from the requestData map
		String permissionName = (String) requestData.get("permissionName");
		String Permission_Desc = (String) requestData.get("Permission_Desc");

		// Check if the group name is already taken
		if (permissionRepository.existsBypermissionName(permissionName)) {
			throw new IllegalArgumentException("Permission name is already taken!");
		}

		// Generate group ID
		long permissionId = sequenceGeneratorService.generateSequence("Permission_Id");
		System.out.println("Generated Group Id: " + permissionId);

		// Create a new group entity and save it
		Permission newPermission = new Permission();
		newPermission.setPermissionId(permissionId);
		newPermission.setPermissionName(permissionName);
		newPermission.setPermission_Desc(Permission_Desc);
		permissionRepository.save(newPermission);
	}
 }
    
    //This method is for Get Permission By Name
    public ResponseEntity<String> getByPermissionName(String permissionName) {
        Permission permission = permissionRepository.findByPermissionName(permissionName);

        if (permission != null) {
            Map<String, Object> responseData = new HashMap<>();
            responseData.put("permissionId", permission.getPermissionId());
            responseData.put("permissionName", permission.getPermissionName());
            responseData.put("permission_Desc", permission.getPermission_Desc());

            String jsonResponse = "{\n" +
                    "  \"permissionId\": \"" + permission.getPermissionId() + "\",\n" +
                    "  \"permissionName\": \"" + permission.getPermissionName() + "\",\n" +
                    "  \"permission_Desc\": \"" + permission.getPermission_Desc() + "\"\n" +
                    "}";

            return ResponseEntity.ok(jsonResponse);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Application not found for name: " + permissionName);
        }
    }

    //This method is for Update Permission by Name
    public ResponseEntity<String> updateByPermissionName(String permissionName, Map<String, Object> requestData) {
        Permission existingApp = permissionRepository.findByPermissionName(permissionName);

        if (existingApp != null) {
            // Update the application properties based on the requestData
            // For example, assuming "appDesc" can be updated:
            if (requestData.containsKey("permission_Desc")) {
                String newAppDesc = (String) requestData.get("permission_Desc");
                existingApp.setPermission_Desc(newAppDesc);
                // Save the updated application
                permissionRepository.save(existingApp);

                // Prepare and return the response
                Map<String, Object> responseData = new HashMap<>();
                responseData.put("permissionId", existingApp.getPermissionId());
                responseData.put("permissionName", existingApp.getPermissionName());
                responseData.put("permission_Desc", existingApp.getPermission_Desc());

                String jsonResponse = "{\n" +
                        "  \"permissionId\": \"" + existingApp.getPermissionId() + "\",\n" +
                        "  \"permissionName\": \"" + existingApp.getPermissionName() + "\",\n" +
                        "  \"permission_Desc\": \"" + existingApp.getPermission_Desc() + "\"\n" +
                        "}";
                return ResponseEntity.ok(jsonResponse);
            } else {
                // If "appDesc" is not present in requestData, you can handle it accordingly
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing 'appDesc' in the update request");
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Permission not found for name: " + permissionName);
        }
    }


	
	//This Method is for Delete Permission
	public ResponseEntity<String> deleteByPermissionName(String permissionName) {
	    Permission existingPermission = permissionRepository.findByPermissionName(permissionName);

	    if (existingPermission != null) {
	        // Delete the permission
	        permissionRepository.delete(existingPermission);

	        return ResponseEntity.ok("Permission deleted successfully for name: " + permissionName);
	    } else {
	        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Permission not found for name: " + permissionName);
	    }
	}
	

	
}

