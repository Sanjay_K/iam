package com.ik.iam.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ik.iam.model.Role;
import com.ik.iam.model.User;
import com.ik.iam.repository.RoleRepository;
import com.ik.iam.repository.UserRepository;

@Service
public class RoleToUser {
	
	private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public RoleToUser(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    } 
    
    
    //This method is to Assign Roles to User
    @Transactional
    public ResponseEntity<String> assignRolesToUser(Map<String, Object> requestData) {
        // Extract roleName and usersData from the request data
        String roleName = (String) requestData.get("roleName");
        List<Map<String, String>> usersData = (List<Map<String, String>>) requestData.get("users");

        // Check for missing data in the request
        if (roleName == null || usersData == null) {
            return ResponseEntity.badRequest().body("Missing 'roleName' or 'users' in request data");
        }

        // Fetch the role from the repository based on roleName
        Role role = roleRepository.findByRoleName1(roleName);

        // Check if the role exists
        if (role == null) {
            return ResponseEntity.badRequest().body("Role not found");
        }

        List<String> errorMessages = new ArrayList<>();

        // Loop through each user in usersData
        for (Map<String, String> userData : usersData) {
            String userName = userData.get("name");

            // Fetch the user from the repository based on userName
            User user = userRepository.findByname(userName);

            // Check if the user exists and is active
            if (user != null && user.isAccountStatus()) {
                // If the user is active, add the role to the user's roles
                List<Role> userRoles = user.getRoles();

                // Initialize userRoles if null
                if (userRoles == null) {
                    userRoles = new ArrayList<>();
                }

                // Check if the user already has the role
                if (userRoles.stream().noneMatch(r -> r.getRoleName().equals(roleName))) {
                    // If the role is not already assigned, add it
                    userRoles.add(role);
                    user.setRoles(userRoles);
                    userRepository.save(user);
                } else {
                    // Store error message for the user
                    errorMessages.add("Role '" + roleName + "' already assigned to user '" + userName + "'");
                }
            } else {
                // Log or handle inactive users as needed
            }
        }

        // Return appropriate response based on errorMessages
        if (!errorMessages.isEmpty()) {
            // Return error response if there are error messages
            return ResponseEntity.badRequest().body(errorMessages.toString());
        } else {
            // Return success response if no errors occurred
            return ResponseEntity.ok("Roles assigned successfully");
        }
    }

    
    //This method is for get the User with Roles by username
    public ResponseEntity<Map<String, Object>> getUserAndRolesByUsername(String username) {
        Optional<User> userOptional = userRepository.findByname1(username);

        if (userOptional.isPresent()) {
            User user = userOptional.get();

            // Fetch roles for the user
            List<Role> roles = user.getRoles();

            // Build a response map containing user and role information
            Map<String, Object> response = new HashMap<>();
            response.put("user", user);
//            response.put("roles", roles);

            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    
    
    //This method is to Remove the Role From User
    @Transactional
    public ResponseEntity<String> removeUserFromRole(Map<String, Object> requestData) {
        String roleName = (String) requestData.get("roleName");
        String userName = (String) requestData.get("userName");

        if (roleName == null || userName == null) {
            return ResponseEntity.badRequest().body("Missing 'roleName' or 'userName' in request data");
        }

        Role role = roleRepository.findByRoleName1(roleName);

        if (role == null) {
            return ResponseEntity.badRequest().body("Role not found");
        }

        User user = userRepository.findByname(userName);

        if (user != null) {
            List<Role> userRoles = user.getRoles();

            if (userRoles != null && userRoles.removeIf(r -> r.getRoleName().equals(role.getRoleName()))) {
                user.setRoles(userRoles);
                userRepository.save(user);
                return ResponseEntity.ok("User removed from role successfully");
            } else {
                return ResponseEntity.badRequest().body("User '" + userName + "' does not have role '" + roleName + "'");
            }
        } else {
            return ResponseEntity.badRequest().body("User '" + userName + "' not found");
        }
    }

    
}
