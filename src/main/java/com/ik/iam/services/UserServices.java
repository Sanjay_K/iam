package com.ik.iam.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.ik.iam.model.Application;
import com.ik.iam.model.Policy;
import com.ik.iam.model.Role;
import com.ik.iam.model.User;
import com.ik.iam.repository.ApplicationRepository;
import com.ik.iam.repository.PolicyRepository;
import com.ik.iam.repository.RoleRepository;
import com.ik.iam.repository.UserRepository;

@Service
public class UserServices  {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@Autowired
	private PolicyRepository policyRepository;
	

    @Autowired
    private ApplicationRepository applicationRepository;
	
	//This method is to GetUsersById
	public ResponseEntity<?> getUsersById(Long userId) {
		Optional<User> optionalUser = userRepository.findByUser_Id(userId);

		return optionalUser.map(user -> {
			System.out.println("User details: " + user.getEmail());
			System.out.println("User details: " + user.getId());
			System.out.println("User details: " + user.getUserId());

			System.out.println("User details: " + user.getName());
//			System.out.println("User details: " + user.getCompanyName());

			return ResponseEntity.ok(user);
		}).orElseThrow();
	}
	
	
	
	// This method is to GetUserByName
	public ResponseEntity<Object> getUsersByName(String name) {
		try {
			List<User> usersList = userRepository.findBynameIgnoreCase(name.trim());

			if (!usersList.isEmpty()) {
				List<Map<String, Object>> userList = usersList.stream().map(user -> {
					Map<String, Object> userMap = new HashMap<>();
					userMap.put("userId", user.getUserId());
					userMap.put("name", user.getName());
//					userMap.put("companyName", user.getCompanyName());
					userMap.put("email", user.getEmail());
					// Add other fields as needed
					return userMap;
				}).collect(Collectors.toList());

				return ResponseEntity.ok(userList);
			} else {
				System.out.println("No users found for name: " + name);
				// Handle the case where no users are found
				Map<String, String> responseMap = Collections.singletonMap("message",
						"No users found for name: " + name);
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
			}
		} catch (Exception e) {
			// Log any exceptions for debugging
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(Collections.singletonMap("error", e.getMessage()));
		}
	}
	
	
	
	//This method is to GetAllUsers
	public ResponseEntity<Object> getAllUsers() {
		try {
			List<User> allUsers = userRepository.findAll();

			if (!allUsers.isEmpty()) {
				List<Map<String, Object>> userList = allUsers.stream().map(user -> {
					Map<String, Object> userMap = new HashMap<>();
					userMap.put("userId", user.getUserId());

					// Check for null values before adding to the map
					if (user.getName() != null) {
						userMap.put("name", user.getName());
					}

					

					if (user.getEmail() != null) {
						userMap.put("email", user.getEmail());
					}

					// Add other fields as needed
					return userMap;
				}).collect(Collectors.toList());

				// Log the response body
//				System.out.println("Response Body: " + userList);

				return ResponseEntity.ok(userList);
			} else {
				System.out.println("No users found.");
				// Handle the case where no users are found
				Map<String, String> responseMap = Collections.singletonMap("message", "No users found.");
//				System.out.println("Response Body: " + responseMap);
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMap);
			}
		} catch (Exception e) {
			// Log any exceptions for debugging
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(Collections.singletonMap("error", e.getMessage()));
		}
	}
	
	
	
	//This method is to Update User by Name
	@Transactional
    public User updateUserByName(String name, Map<String, Object> requestData) throws NotFoundException {
        // 1. Check if user exists by name
		User existingUser = userRepository.findByname(name);
        if (existingUser == null) {
            throw new NotFoundException();
        }

        // 2. Validate and apply updates from request data
        for (Map.Entry<String, Object> entry : requestData.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            // Check only specific attributes to update
            switch (key) {
                case "email":
                    existingUser.setEmail((String) value);
                    break;
                case "password":
                    existingUser.setPassword((String) value);
                    break;
                // Add additional cases for other attributes you want to update
                // case "attributeName":
                //     existingUser.setAttributeName((Type) value);
                //     break;
                default:
                    // Ignore other attributes
                    break;
            }
        }
        // 3. Save the updated user to MongoDB
        return userRepository.save(existingUser);
    }
	
	
	
	//This method is for delete by Username
	 public String deleteUserByUserName(String name) {
		 userRepository.deleteByname(name);
			return "deleted user successfully";
	    }
			 
}




