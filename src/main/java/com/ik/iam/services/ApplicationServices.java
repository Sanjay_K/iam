package com.ik.iam.services;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.ik.iam.model.Application;
import com.ik.iam.repository.ApplicationRepository;
import com.ik.iam.sequnceGenerator.SequenceGeneratorService;

@Service
public class ApplicationServices {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    //This method is for Add Application details
    public void addApp(Map<String, Object> requestData) {
        String appName = (String) requestData.get("appName");
        String appDesc = (String) requestData.get("appDesc");

        if (applicationRepository.existsByAppName(appName)) {
            throw new IllegalArgumentException("App name is already taken!");
        }

        long appId = sequenceGeneratorService.generateSequence("App_Id");

        Application newApp = new Application();
        newApp.setAppId(appId);
        newApp.setAppName(appName);
        newApp.setAppDesc(appDesc);
        applicationRepository.save(newApp);
    }

    public ResponseEntity<String> getByAppName(String appName) {
        Application application = applicationRepository.findByAppName(appName);

        if (application != null) {
            Map<String, Object> responseData = new HashMap<>();
            responseData.put("appId", application.getAppId());
            responseData.put("appName", application.getAppName());
            responseData.put("appDesc", application.getAppDesc());

            String jsonResponse = "{\n" +
                    "  \"appId\": \"" + application.getAppId() + "\",\n" +
                    "  \"appName\": \"" + application.getAppName() + "\",\n" +
                    "  \"appDesc\": \"" + application.getAppDesc() + "\"\n" +
                    "}";

            return ResponseEntity.ok(jsonResponse);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Application not found for name: " + appName);
        }
    }
    
    //This Method is for Updating Application by Name
    @Transactional
    public Application updateApplicationByName(String name, Map<String, Object> requestData) throws NotFoundException {
        // 1. Check if application exists by name
        Application existingApplication = applicationRepository.findByAppName(name);
        if (existingApplication == null) {
            throw new NotFoundException();
        }

        // 2. Validate and apply updates from request data
        for (Map.Entry<String, Object> entry : requestData.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            // Check only specific attributes to update, matching model fields
            switch (key) {
                case "appId":
                    existingApplication.setAppId((Long) value);  // Use Long for appId
                    break;
                case "appDesc":
                    existingApplication.setAppDesc((String) value);
                    break;
                // Add additional cases for other attributes you want to update
                // case "attributeName":
                //     existingApplication.setAttributeName((Type) value);
                //     break;
                default:
                    // Ignore other attributes
                    break;
            }
        }

        // 3. Save the updated application to MongoDB
        return applicationRepository.save(existingApplication);
    }

    
    
    //This Method is for Deleting Application By Name
    public String deleteByAppName(String appName) {
        Application existingApp = applicationRepository.findByAppName(appName);

        if (existingApp != null) {
            // Delete the application
            applicationRepository.delete(existingApp);   
        }
        return "Application deleted successfully! ";
    }
    
	
}