package com.ik.iam.apiConfigs;

public class ActionConfig {
	
	 private String api;
	    private String endpoint;
	    private String method;
	    private String serviceClass;
	    private String serviceMethod;
	    private String entity;
	    // Constructors, getters, and setters

	    public ActionConfig() {
	    }

	    public ActionConfig(String api, String endpoint, String method, String serviceClass, String serviceMethod,String entity) {
	        this.api= api;
	        this.endpoint = endpoint;
	        this.method = method;
	        this.serviceClass = serviceClass;
	        this.serviceMethod = serviceMethod;
	        this.entity=entity;
	    }

	    public String getapi() {
	        return api;
	    }

	    public void setapi(String api) {
	        this.api= api;
	    }

	    public String getendpoint() {
	        return endpoint;
	    }

	    public void setendpoint(String endpoint) {
	        this.endpoint = endpoint;
	    }

	    public String getMethod() {
	        return method;
	    }

	    public void setMethod(String method) {
	        this.method = method;
	    }

	    public String getServiceClass() {
	        return serviceClass;
	    }

	    public void setServiceClass(String serviceClass) {
	        this.serviceClass = serviceClass;
	    }

	    public String getServiceMethod() {
	        return serviceMethod;
	    }

	    public void setServiceMethod(String serviceMethod) {
	        this.serviceMethod = serviceMethod;
	    }
	    
	    
	    public String getentity() {
	    	return entity;
	    }
	    public void setentity(String entity) {
	    	this.entity=entity;
	    }

		
	    
	    
	    
	    
	    
}