package com.ik.iam.apiConfigs;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
//@EnableConfigurationProperties
@ConfigurationProperties("app-prop")
public class ApiConfigProperties {
	private List<ActionConfig> actions;

	public ApiConfigProperties() {
	}

	public ApiConfigProperties(List<ActionConfig> actions) {
		this.actions = actions;
	}

	public List<ActionConfig> getActions() {
		return actions;
	}

	public void setActions(List<ActionConfig> actions) {
		this.actions = actions;
	}
//	 @PostConstruct
//	    public void init() {
//	    	System.out.println(actions);
//	    	  for (ActionConfig action : actions) {
//	              System.out.println("api:" + actions.get("api"));
//	              
//	          }	    }
	
	
	@PostConstruct
	public void init() {
	    System.out.println("Actions:");
	    for (ActionConfig action : actions) {
	     System.out.println("Api:" + action.getapi());
	     System.out.println("Model:" +action.getentity());
	     
	     
	    }
	}

}
