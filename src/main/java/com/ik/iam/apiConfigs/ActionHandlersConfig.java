package com.ik.iam.apiConfigs;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActionHandlersConfig {

    @Bean
    public Map<String, Consumer<Map<String, Object>>> actionHandlers() {
        return new HashMap<>();
    }
}
