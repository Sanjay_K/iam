package com.ik.iam.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.ik.iam.repository.UserRepository;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })

@ComponentScan(basePackages = "com.ik.iam.apiConfigs")
@EntityScan(basePackages = "com.ik.iam.model")
@ComponentScan("com.ik.iam")
@ComponentScan(basePackages = "com.ik.iam.services")
@ComponentScan(basePackages = "com.ik.iam.security")
@ComponentScan(basePackages = "com.ik.iam.validationConfigs")
@Configuration
@ComponentScan(basePackages = "com.ik.iam.mapper")
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@ComponentScan
@ComponentScan(basePackages = "com.ik.iam.repository")
public class IdentityAccessMangementApplication {

	public static void main(String[] args) {
		SpringApplication.run(IdentityAccessMangementApplication.class, args);
	}

}
